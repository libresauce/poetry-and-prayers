<!--Based on Psalm 72:1-7- Other references: Revelation 19:7-8, Matthew 26:29-->
O God, grant us your love for justice,\
and your wisdom for right living.\
Show us how to judge through your eyes,\
and heal with your gentle hand.\
Have your way in this land;\
rescue the oppressed,\
defend the poor,\
heal the broken.\
May the rule of your bride be as a stream in the desert,\
and as a warming fire in the dead of winter.\
Prosper us until we are seated at your wedding feast,\
drinking the new wine of your Kingdom.
