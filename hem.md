Merciful God,\
We offer our thanks for the gifts you have granted us\
and for the promises revealed to us, our faith and surrender.\
Your co-suffering love gives us confidence these needs and more will be provided for.\
We release these cares into your hands, and take hold of the hem of your garment, knowing you will once again work a miracle in and among us.
