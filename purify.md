All-seeing God,\
Even darkness is not dark to you.\
The hearts of men are laid open before you.\
Grant that we may be willing partners\
	as your Word cuts between soul and spirit.\
Let us not shrink back from your presence in fear,\
but knowing perfect love casts out fear,\
seek it all the more.\
Purify us in thought, word, and deed\
That we may be light and salt\
to a world in need.\
To the glory of God,\
	who is Father, Son, and Spirit.\
Amen.
