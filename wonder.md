To be childlike is to wonder.\
To wonder what is; what could be.\
To wonder who I am; who God is.\
Mystery becomes a tangible, commanding force.\
A taste of something you can't swallow;\
	the touch of something you can't hold.\
A reminder that we know in part,\
	we see in part,\
	we appreciate in part.\
Commanding our internal narratives to silence\
as we awake to a narrative grander than we know,\
	more dazzling that we see,\
	more encompassing than we appreciate.\
The narrative, that for some reason, includes us.\
The narrative, that for some reason, includes dust.\
What mysteries have the Author comprehended?\
	What beauty have His eyes beheld?\
	What worlds has He fathomed?\
I wonder; does He?
